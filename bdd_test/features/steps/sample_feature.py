from behave import *

use_step_matcher("re")


@given("Kalkulator jest wlaczony")
def step_impl(context):
    context.liczby = []
    """
    :type context: behave.runner.Context
    """
    pass


@when("Wprowadze 1")
def step_impl(context):
    context.liczby.append(1)
    """
    :type context: behave.runner.Context
    """
    pass


@when("Wprowadze 2")
def step_impl(context):
    context.liczby.append(2)
    """
    :type context: behave.runner.Context
    """
    pass

@step("Wprowadze \+")
def step_impl(context):
    context.liczby.append("+")
    """
    :type context: behave.runner.Context
    """
    pass


@step("Wprowadze =")
def step_impl(context):
    liczby = context.liczby
    """
    :type context: behave.runner.Context
    """
    context.wynik = sum(liczby)


@then("Wynik to 3")
def step_impl(context):
    assert context.wynik == 3


@when("Wprowadze '(?P<liczba>.+)'")
def step_impl(context, liczba):
    context.liczby.append(int(liczba))



@then("Wynik to '(?P<wynik>.+)'")
def step_impl(context, wynik):
    assert context.wynik == int(wynik), "Wynik oczekiwany to %s faktyczny to %s" % (wynik, context.wynik)