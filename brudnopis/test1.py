"""
wstęp
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

#setup
driver = webdriver.Firefox()
driver.get("http://google.pl")

try:
    #test
    element_locator = (By.XPATH, "//div[@class='sfibbbc']")
    button_locator = (By.CLASS_NAME, "lsb")
    expected_links = (By.XPATH, "//h3")

    a = driver.find_element(*element_locator)
    a.send_keys("python")
    b = driver.find_element(*button_locator)
    b.click()
    time.sleep(2)

    # time.sleep(10)
    test = driver.find_elements(*expected_links)

    assert len(test) > 0

except AssertionError:

    #teardown
    driver.close()

    raise