class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def click_on(self, button_name):
        element = self.get_element(button_name, type="button")
        element.click()

    def fill_input(self, name, value):
        element = self.get_element(name, type="imput")
        element.send_keys(value)

    def get_element(self, name, type):
        name = name + "_" + type
        locator = self.locators.get(name)
        element = self.driver.find_element(*locator)
        return element


class MyPage(BasePage):
    locators = {
        "imie_nazwisko_input": ["xpath", "//input"],
        "submit_button": ["id", "//button"]
    }

    def fill_imie_nazwisko(self, wartosc):
        self.fill_input("imie_nazwisko", wartosc)

    def click_submit(self):
        self.click_on("submit")


"""
test1
setup:
driver = webdriver.Firefox()
my_page = MyPage(driver)
my_page.open()

test:
my_page.click_submit()
assert czy jestes na tej samej stronie

tear_down:
driver.quit()
"""
