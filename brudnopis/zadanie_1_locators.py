from selenium.webdriver.common.by import By

produkt_xpath = "//div[@class='caption']/h4[contains(.,'%s')]/.."

locators = {
    "produkt_ilosc": (By.XPATH, produkt_xpath + "/div/input"),
    "produkt_cena": (By.XPATH, produkt_xpath + "/p[1]"),
    "produkt_opis": (By.XPATH, produkt_xpath + "/p[2]"),
    "produkt_dodaj": (By.XPATH, produkt_xpath + "/span/button")
}
