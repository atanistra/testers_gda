import time

from brudnopis.zadanie_1_locators import locators
from selenium import webdriver


class BasePage:
    def __init__(self):
        self.driver = webdriver.Chrome()

    def open_page(self):
        self.driver.get("https://testingcup.pgs-soft.com/task_1")

    def get_title(self):
        pass

    def znajdz_element(self, locator):
        element = self.driver.find_element(*locator)
        return element

    def kliknij_przycisk(self, locator):
        pass

    def wypelnij_pole(self, locator, text):
        """
         znajdz_elemnt
        wyczysc_element
        wypełnij"""
        element = self.znajdz_element(locator)
        element.clear()
        element.send_keys(text)

        pass


class Zadanie1Page(BasePage):

    def dodaj_produkt(self, nazwa_produktu, ilosc_produktu):
        locator = locators.get("Przykladowy lokator")


    def usun_produkt(self, nazwa):
        pass

    def zwieksz_ilosc_produktu_strzalkami(self, nazwa_produktu, ilosc_klikniec):
        pass

    def zmniejsz_ilosc_produktu_strzalkami(self, nazwa_produktu, ilosc_klikniec):
        pass

    def wpisz_ilosc_produktu(self, nazwa_produktu, ilosc):
        locator = locators.get("produkt_ilosc")
        xpath_value = locator[1] % nazwa_produktu
        locator = (locator[0], xpath_value)
        self.wypelnij_pole(locator, ilosc)


    def kilknij_dodaj(self, nazwa_produktu):
        locator = locators.get("produkt_dodaj")
        xpath_value = locator[1] % nazwa_produktu
        locator = (locator[0], xpath_value)


# probny test
test_driver = ""
page = Zadanie1Page()
page.open_page()
page.wpisz_ilosc_produktu("Okulary", 32)
page.kliknij_dodaj()