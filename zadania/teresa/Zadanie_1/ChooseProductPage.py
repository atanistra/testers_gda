from selenium import webdriver
from selenium.webdriver.common.by import By


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def click_on(self, button_name):
        element = self.get_element(button_name, type="button")
        element.click()

    def fill_input(self, name, value):
        element = self.get_element(name, type="imput")
        element.send_keys(value)

    def get_element(self, name, type):
        name = name + "_" + type
        locator = self.locators.get(name)
        element = self.driver.find_element(*locator)
        return element

    def open(self):
        self.driver.get(self.url)

class ChooseProductPage(BasePage):
    url = "https://testingcup.pgs-soft.com/task_1"

    def incrementProductCounter(self, ile=5):
        inputElement = driver.find_element(By.XPATH, "//h4[text()='Kubek']/..//input")
        # inputElement.click()
        inputElement.send_keys(ile)

    def addKubek(self):

        ButtonElement = driver.find_element(By.XPATH, "//h4[text()='Kubek']/..//button")
        ButtonElement.click()

    def validateSummaryQuantity(self):

        SummaryQuantityElement = driver.find_element(By.XPATH, "//span[@class='summary-quantity']")
        print("Actual summary quantity:" + SummaryQuantityElement.text)
        assert SummaryQuantityElement.text == "5"

#TEST

# def test1():
    # setup:
driver = webdriver.Firefox()
chooseProductPage = ChooseProductPage(driver)
chooseProductPage.open()

chooseProductPage.incrementProductCounter()
chooseProductPage.addKubek()
chooseProductPage.validateSummaryQuantity()


    # test:
    # my_page.click_submit()
    # assert czy
    # jestes
    # na
    # tej
    # samej
    # stronie
    #
    # tear_down
# driver.quit()